<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

return [
	'adminer_description' => 'Utiliser Adminer sur la base SPIP',
	'adminer_slogan' => 'Utiliser Adminer pour explorer la base SPIP',
	'adminer_nom' => 'Adminer'
];
