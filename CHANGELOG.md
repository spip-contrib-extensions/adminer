# Changelog du plugin Adminer

## 5.0.0 — 2025-02-25

### Changed

- Compatible SPIP 4.2 minimum
- Adminer version 4.17.1
